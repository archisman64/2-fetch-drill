
/*
NOTE: Do not change the name of this file

* Ensure that error handling is well tested.
* If there is an error at any point, the subsequent solutions must not get executed.
* Solutions without error handling will get rejected and you will be marked as having not completed this drill.

* Usage of async and await is not allowed.

Users API url: https://jsonplaceholder.typicode.com/users
Todos API url: https://jsonplaceholder.typicode.com/todos

Users API url for specific user ids : https://jsonplaceholder.typicode.com/users?id=2302913
Todos API url for specific user Ids : https://jsonplaceholder.typicode.com/todos?userId=2321392

Using promises and the `fetch` library, do the following. 

1. Fetch all the users
2. Fetch all the todos
3. Use the promise chain and fetch the users first and then the todos.
4. Use the promise chain and fetch the users first and then all the details for each user.
5. Use the promise chain and fetch the first todo. Then find all the details for the user that is associated with that todo

NOTE: If you need to install `node-fetch` or a similar libary, let your mentor know before doing so along with the reason why. No other exteral libraries are allowed to be used.

Usage of the path libary is recommended


*/

const fs = require('fs');
const path = require('path');
const fetch = require('node-fetch');

const pathToDataDir = path.join(__dirname, 'data');
const usersFilePath = path.join(pathToDataDir, 'users.txt');
const todosFilePath = path.join(pathToDataDir, 'todos.txt');

//  1)
function getUsers() {
    return new Promise((resolve, reject) => {

        fetch('https://jsonplaceholder.typicode.com/users')
            .then((response) => {
                console.log('got users data from url');
                return response.json()
            })
            .then((data) => {
                // console.log(data)
                resolve(data);
            })
            .catch((error) => {
                reject(error);
            })
    })
}

//  2)
function getTodos() {
    return new Promise((resolve, reject) => {
        fetch('https://jsonplaceholder.typicode.com/todos')
            .then((response) => {
                console.log('got todos data from url');
                return response.json()
            })
            .then((data) => {
                resolve(data);
            })
            .catch((error) => {
                reject(error);
            })
    })
}

function writeToFile(path, data) {
    return new Promise((resolve, reject) => {
        fs.writeFile(path, data, (err) => {
            if(err) {
                reject(err);
            } else {
                resolve(`successfully written to ${path}`);
            }
        })
    });
}

//  3)
getUsers()
    .then((result) => {
        // console.log('result 1:', result);
        return writeToFile(usersFilePath, JSON.stringify(result));
    })
    .then((statement) => {
        console.log(statement);
        return getTodos();
    })
    .then((result) => {
        // console.log('result 3:', result);
        return writeToFile(todosFilePath, JSON.stringify(result));
    })
    .then((statement) => {
        console.log(statement);
    }) 
    .catch((err) => {
        console.log(err);
    });
